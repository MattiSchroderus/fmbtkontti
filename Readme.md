## 1. Asenna Docker: https://docs.docker.com/install/linux/docker-ce/ubuntu/#install-docker-ce-1
## 2. sudo docker run --privileged -v ~/tests:/tests -e TEST=test.conf -e TESTREPOSITORY=https://github.com/intel/fMBT.git -e TESTLOCATION=fMBT/examples/python-unittest -e OUTPUTDIR=/tests registry.gitlab.com/mattischroderus/fmbtkontti:latest


### Yllä olevassa esimerkissä haetaan repositoriosta testi, ajetaan se ja saadaan tulokset /tests.



### fMBT:n manuaalinen käyttö:   sudo docker run -it registry.gitlab.com/mattischroderus/fmbtkontti:latest /bin/bash



### TEST = testitiedosto
### TESTREPOSITORY = testien repositorio
### TESTLOCATION = testin sijainti, repositorion nimi
### TESTLOCATION = test.log:in sijoituspaikka, tässä tapauksessa Volyymi/Bind Mountti /tests

