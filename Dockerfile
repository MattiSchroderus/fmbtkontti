FROM ubuntu:latest


WORKDIR /app


#           Muiden ohjelmien asennus


RUN     apt-get update
RUN     apt-get -y install apt-utils
RUN     DEBIAN_FRONTEND=noninteractive apt-get -y install software-properties-common
RUN     apt-get -y update
RUN     DEBIAN_FRONTEND=noninteractive apt-get -y install build-essential 
RUN     apt-get -y install git
RUN     DEBIAN_FRONTEND=noninteractive apt-get -y install libglib2.0-dev
RUN     DEBIAN_FRONTEND=noninteractive apt-get -y install libboost-regex-dev
RUN     DEBIAN_FRONTEND=noninteractive apt-get -y install libedit-dev
RUN     apt-get -y install libmagickcore-dev
RUN     DEBIAN_FRONTEND=noninteractive apt-get -y install python-dev
RUN     DEBIAN_FRONTEND=noninteractive apt-get -y install python-pexpect
RUN     DEBIAN_FRONTEND=noninteractive apt-get -y install python-dbus
RUN     DEBIAN_FRONTEND=noninteractive apt-get -y install python-gobject
RUN     DEBIAN_FRONTEND=noninteractive apt-get -y install gawk
RUN     DEBIAN_FRONTEND=noninteractive apt-get -y install libtool
RUN     DEBIAN_FRONTEND=noninteractive apt-get -y install autoconf
RUN     DEBIAN_FRONTEND=noninteractive apt-get -y install automake
RUN     DEBIAN_FRONTEND=noninteractive apt-get -y install debhelper
RUN     DEBIAN_FRONTEND=noninteractive apt-get -y install libboost-dev
RUN     DEBIAN_FRONTEND=noninteractive apt-get -y install flex
RUN     DEBIAN_FRONTEND=noninteractive apt-get -y install libpng12-0
RUN     DEBIAN_FRONTEND=noninteractive apt-get -y install libxml2-dev
RUN     DEBIAN_FRONTEND=noninteractive apt-get -y install graphviz
RUN     DEBIAN_FRONTEND=noninteractive apt-get -y install imagemagick
RUN     DEBIAN_FRONTEND=noninteractive apt-get -y install gnuplot
RUN     DEBIAN_FRONTEND=noninteractive apt-get -y install python-pyside
RUN     DEBIAN_FRONTEND=noninteractive apt-get -y install tesseract-ocr
RUN     DEBIAN_FRONTEND=noninteractive apt-get -y -q install xinit
RUN     DEBIAN_FRONTEND=noninteractive apt-get -y -q install wget


#           fMBT asennus

RUN     git clone https://github.com/01org/fMBT.git



ADD . /app




WORKDIR /app/fMBT


RUN     ls
RUN     ./autogen.sh
RUN     ./configure
RUN     make
RUN     make install
RUN     DEBIAN_FRONTEND=noninteractive apt-get -y install python-pip firefox xvfb
RUN     pip install selenium
RUN     wget https://github.com/mozilla/geckodriver/releases/download/v0.19.1/geckodriver-v0.19.1-linux64.tar.gz
RUN     tar xvzf geckodriver-v0.19.1-linux64.tar.gz

#RUN     export PATH=$PATH:/app/geckodriver

#RUN     export PATH=$PATH:/app/start.sh

#RUN     mv /app/start.sh /usr/local/bin

RUN     ls
RUN     mv geckodriver /usr/local/bin
RUN     ls /usr/local/bin
COPY    start.sh /usr/local/bin/start.sh
RUN     chmod +x /usr/local/bin/start.sh
RUN     ls /usr/local/bin

###################################################


CMD ["start.sh"]


###################################################






